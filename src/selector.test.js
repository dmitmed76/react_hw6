import {
	selectProjectors, selectIsModal, selectIsModalTwo,
	selectFavorites, selectCurrentGoods, selectBasket
} from './store/selectors';

const mockCards = [{
	"title": "Touyinger H6 FullHD, Android version", "price": 4400,
	"url": "https://images.prom.ua/4737287106_akkumulyator-gelevyj.jpg",
	"article": 23456, "color": "White", "isFavorite": false
}]

describe('test selectors', () => {
	it('test selector selectProjectors', () => {
		expect(selectProjectors({
			goods: {
				projectors: [{
					"title": "Touyinger H6 FullHD, Android version", "price": 4400,
					"url": "https://images.prom.ua/4737287106_akkumulyator-gelevyj.jpg",
					"article": 23456, "color": "White", "isFavorite": false
				}]
			}
		})).toEqual(mockCards)
	});

	it('test selector selectIsModal', () => {
		expect(selectIsModal({
			isModal: {
				isModal: false
			}
		})).toBe(false);
	});
	it('test selector selectIsModaTwo', () => {
		expect(selectIsModalTwo({
			isModalTwo: {
				isModalTwo: false
			}
		})).toBe(false);
	});

	it('test selector selectFavorites', () => {
		expect(selectFavorites({
			isFavorite: {
				favorites: [{
					"title": "Touyinger H6 FullHD, Android version", "price": 4400,
					"url": "https://images.prom.ua/4737287106_akkumulyator-gelevyj.jpg",
					"article": 23456, "color": "White", "isFavorite": false
				}]
			}
		})).toEqual(mockCards)
	});

	it('test selector selectBasket', () => {
		expect(selectBasket({
			isBasket: {
				basket: [{
					"title": "Touyinger H6 FullHD, Android version", "price": 4400,
					"url": "https://images.prom.ua/4737287106_akkumulyator-gelevyj.jpg",
					"article": 23456, "color": "White", "isFavorite": false
				}]
			}
		})).toEqual(mockCards)
	});

	it('test selector selectCurrentGoods', () => {
		const mockCurrentGoods = {
			"title": "Touyinger H6 FullHD, Android version", "price": 4400,
			"url": "https://images.prom.ua/4737287106_akkumulyator-gelevyj.jpg",
			"article": 23456, "color": "White", "isFavorite": false
		}
		expect(selectCurrentGoods({
			isBasket: {
				currentGoods: {
					"title": "Touyinger H6 FullHD, Android version", "price": 4400,
					"url": "https://images.prom.ua/4737287106_akkumulyator-gelevyj.jpg",
					"article": 23456, "color": "White", "isFavorite": false
				}
			}
		})).toEqual(mockCurrentGoods)
	});
});
