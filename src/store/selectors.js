export const selectProjectors = ((state) => state.goods.projectors);
export const { status, error } = ((state) => state.goods);
export const selectIsModal = ((state) => state.isModal.isModal);
export const selectIsModalTwo = ((state) => state.isModalTwo.isModalTwo);
export const selectFavorites = ((state) => state.isFavorite.favorites);
export const selectCurrentGoods = ((state) => state.isBasket.currentGoods);
export const selectBasket = ((state) => state.isBasket.basket);