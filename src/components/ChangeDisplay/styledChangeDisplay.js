import styled from 'styled-components';

const BasketCardsWrapper = styled.div`
display: -webkit-box;
width: 100%;
align-items: center;
justify-content: space-around;
gap: 50px;
margin: 20px 20px 20px 50px;
box-shadow: 5px 5px 10px grey;
border-radius: 5px;
&:hover{
	box-shadow: 5px 5px 10px darkblue;
}
`

const CardWrapper = styled.div`
position: relative;
box-shadow: 5px 5px 10px grey;
border-radius: 5px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: space-between;
&:hover{
	box-shadow: 5px 5px 10px darkblue;
}
`
const CardsWrapper = styled.div`
display: -webkit-box;
grid-template-columns: none;
gap: 20px;
padding: 20px;
`

export { CardsWrapper, CardWrapper, BasketCardsWrapper };