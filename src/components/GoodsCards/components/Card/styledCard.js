import styled from 'styled-components';

const CardTitle = styled.p`
padding: 10px 0 0 0;
text-align: center;
font-size: 18px;
font-weight: 700;
`

const GoodsColor = styled.p`
padding: 10px 0 0 0;
font-size: 14px;
font-weight: 700;
`

const CardArt = styled.p`
font-size: 12px;
padding: 5px 0 0 0;
`

const CardImg = styled.img`
width: 200px;
height: 200px;
`

const CardPrice = styled.p`
padding: 0 0 10px 0;
text-align: center;
font-size: 16px;
font-weight: 700;
`

const CardStar = styled.button`
position: absolute;
bottom: 25px;
right: 25px;
cursor: pointer;
border: none;
background-color: transparent;
`

const ButtonStyle = styled.button`
background-color: darkblue;
color: white;
text-transform: uppercase;
padding: 15px 25px;
border: none;
border-radius: 5px;
cursor: pointer;
margin: 0 0 10px 0
`

export { CardTitle, GoodsColor, CardArt, CardImg, CardPrice, CardStar, ButtonStyle };