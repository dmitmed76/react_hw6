import styled from 'styled-components';

import { ReactComponent as Cart } from '../../icons/card.svg';
import { ReactComponent as Table } from '../../icons/table.svg';

const IconsCart = styled(Cart)`
position: absolute;
left: 10px;
top: 80px;
cursor: pointer;
 
`
const IconsTable = styled(Table)`
position: absolute;
left: 10px;
top: 80px;
cursor: pointer;
 
`

export { IconsCart, IconsTable }