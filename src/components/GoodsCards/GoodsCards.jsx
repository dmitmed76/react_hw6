import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { CardsWrapper, CardWrapper } from './styledGoodsCards'
import { Card } from './components/Card';
import { selectProjectors, selectFavorites } from '../../store/selectors';



function GoodsCards(props) {

	const projectors = useSelector(selectProjectors);
	const favorites = useSelector(selectFavorites);
	let cstyle = '';
	if(props.isCart === '1fr'){
		cstyle = '-webkit-box';
	}else{cstyle = '';}

	return (
		
		<CardsWrapper style={{gridTemplateColumns:`${props.isCart}`}}>
			{projectors.map((el, index) => (
				<CardWrapper 
				key={index}
				style = {{display:`${cstyle}`}}
				>
					<Card
						isFavorite={favorites.includes(el.article)}
						item={el}
						// hendlerCurrentGoods={hendlerCurrentGoods}
					/>
				</CardWrapper>
			))}

		</CardsWrapper>

	);

}

GoodsCards.propTypes = {
	hendlerCurrentGoods: PropTypes.func,
}

export { GoodsCards };

