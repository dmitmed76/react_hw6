import { useRef, useEffect } from 'react';
import PropTypes from 'prop-types'

import {
	ModalWindow,
	ModalBody,
	Modalheader,
	CloseButton,
	ModalTitle,
	ModalText,
	ModalFooter,
} from './styledModal'
import { ReactComponent as Closed } from './icons/closed.svg';

function Modal(props) {

	const wrapperRef = useRef();

	useEffect(() => {
		document.addEventListener("mousedown", handleClickOutside);

		return () => {
			document.removeEventListener("mousedown", handleClickOutside)
		};
	})

	const handleClickOutside = (event) => {
		if (wrapperRef && !wrapperRef.current.contains(event.target))
			props.closeButton();
	}

	return (
		<ModalWindow>
			<ModalBody ref={wrapperRef} {...props}>
				<Modalheader>
					<ModalTitle>

						{props.header}

					</ModalTitle>
					<CloseButton onClick={() => props.closeButton()}>
						<Closed />
					</CloseButton>
				</Modalheader>
				<ModalText>

					{props.text}

				</ModalText>
				<ModalFooter />

				{props.actions}

			</ModalBody>
		</ModalWindow>
	)

}

Modal.propTypes = {
	closeButton: PropTypes.func,
	actions: PropTypes.object,
	text: PropTypes.string,
	header: PropTypes.string
}

export default Modal;

