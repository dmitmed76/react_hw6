import PropTypes from 'prop-types';

import { LableStyled, LableTitleStyled,	FieldStyled, ErrorMessegeStyled } from './styledInput'

export default function Input({ error, lable, type, inputName, placeholder, ...restProps }) {

	return (
		<LableStyled  color={error &&'error'}>
			<LableTitleStyled>{lable}</LableTitleStyled>
			<FieldStyled border={error && 'error'}
				type={type}
				name={inputName}
				placeholder={placeholder}
				{...restProps} />
			<ErrorMessegeStyled name={inputName} component="p" />
		</LableStyled>
	)

}

Input.defautProps = {
	type: 'text',
}
Input.propTypes = {
	type: PropTypes.string,
	placeholder: PropTypes.string,
	inputName: PropTypes.string,
	label: PropTypes.string,
	error: PropTypes.object,
}
