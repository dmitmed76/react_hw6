import { toBeInTheDocument } from '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from "react-router-dom";
import userEvent from '@testing-library/user-event';

import App from '../App';
import { store } from '../store';
import { Card } from '../components/GoodsCards/components/Card';

const mockCard = {
	"title": "Набір кухонного приладдя Zepline ZP-053 12 предметів, Силіконовий Бірюзовий",
	"price": 7300,
	"url": "https://images.prom.ua/4183733974_nabor-kuhonnyh-prinadlezhnostej.jpg",
	"article": 12345,
	"color": "Green",
	"isFavorite": false
};

describe('component App', () => {
	it('should open modal', () => {
		const component = render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
					< Card item={mockCard} />
				</MemoryRouter>
			</Provider>
		);
		userEvent.click(screen.getByRole('button', { name: /Add to cart/i }));
		expect(screen.queryByTestId('modal')).toBeInTheDocument();
	});

	it('should closed modal', () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
					< Card item={mockCard} />
				</MemoryRouter>
			</Provider>
		);

		userEvent.click(screen.getByRole('button', { name: /cancel/i }));
		expect(screen.queryByTestId('modal')).toBeNull();
	});

	it('should modal button "ok"', () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
					< Card item={mockCard} />
				</MemoryRouter>
			</Provider>
		);
		expect(screen.queryByTestId('modal')).toBeNull();
		userEvent.click(screen.getByRole('button', { name: /Add to cart/i }));
		expect(screen.queryByTestId('modal')).toBeInTheDocument();
		userEvent.click(screen.getByRole('button', { name: /ok/i }));
		expect(screen.queryByTestId('modal')).toBeNull();
	});
});