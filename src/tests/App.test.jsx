import { toBeInTheDocument } from '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from "react-router-dom";
import userEvent from '@testing-library/user-event';

import App from '../App';
import { store } from '../store';
import { Card } from '../components/GoodsCards/components/Card';

const mockCard = {
	"title": "Touyinger H6 FullHD, Android version", "price": 4400,
	"url": "https://images.prom.ua/4737287106_akkumulyator-gelevyj.jpg",
	"article": 23456, "color": "White", "isFavorite": false
};

describe('component App', () => {
	it('snapshot component App', () => {
		const app1 = render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
				</MemoryRouter>
			</Provider>
		);
		expect(app1).toMatchSnapshot();
	});

	it('Router test', () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
				</MemoryRouter>
			</Provider>
		);
		const linkHome = screen.getByTestId('home-link');
		const linkFavorites = screen.getByTestId('favorites-link');
		const linkBasket = screen.getByTestId('basket-link');

		userEvent.click(linkHome);
		expect(screen.getByTestId('home-page')).toBeInTheDocument();
		userEvent.click(linkFavorites);
		expect(screen.getByTestId('favorites-page')).toBeInTheDocument();
		userEvent.click(linkBasket);
		expect(screen.getByTestId('basket-page')).toBeInTheDocument();
	});
});
