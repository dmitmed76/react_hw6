import { toBeInTheDocument } from '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from "react-router-dom";
import userEvent from '@testing-library/user-event';
import * as reduxHooks from 'react-redux';

import App from '../App';
import { store } from '../store';
import { BasketCard } from '../pages/Basket/components/BasketCard';


const mockCard = {
	"title": "Touyinger H6 FullHD, Android version", "price": 4400,
	"url": "https://images.prom.ua/4737287106_akkumulyator-gelevyj.jpg",
	"article": 23456, "color": "White", "isFavorite": false
};

describe('component App', () => {
	it('should open modal', () => {
		const component = render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
					< BasketCard el={mockCard} />
				</MemoryRouter>
			</Provider>
		);


		userEvent.click(screen.getByTestId('delete-basket'));
		expect(screen.queryByTestId('modal-basket')).toBeInTheDocument();
	});

	it('should closed modal', () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
					< BasketCard el={mockCard} />
				</MemoryRouter>
			</Provider>
		);

		userEvent.click(screen.getByRole('button', { name: /cancel/i }));
		expect(screen.queryByTestId('modal-basket')).toBeNull();
	});

	it('should modal button "ok"', () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
					< BasketCard el={mockCard} />
				</MemoryRouter>
			</Provider>
		);
		expect(screen.queryByTestId('modal-basket')).toBeNull();
		userEvent.click(screen.getByTestId('delete-basket'));
		expect(screen.queryByTestId('modal-basket')).toBeInTheDocument();
		userEvent.click(screen.getByRole('button', { name: /ok/i }));
		expect(screen.queryByTestId('modal-basket')).toBeNull();
	});
});