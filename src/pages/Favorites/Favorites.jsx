import PropTypes from 'prop-types';
import { ReactComponent as FavoritStar } from '../../components/GoodsCards/components/Card/icons/favoritStar.svg';
import { useDispatch, useSelector } from 'react-redux';
import { actionAddIsFavorite, actionRemoveIsFavorites } from '../../features/favorites/favoritesSlice';

import {
	FavoritesCardTitle,
	FavoritesGoodsColor,
	FavoritesCardArt,
	FavoritesCardImg,
	FavoritesCardPrice,
	FavoritesCardsWrapper,
	FavoritesCardStar
} from './styledFavorites'

export default function Favorites({ favorites }) {
	const dispatch = useDispatch();
	const hendlerFavorites = (item) => {
		console.log(item);
		const isAdd = favorites.some((el) => (el.article === item.article));
		if (isAdd) {
			dispatch(actionRemoveIsFavorites(item));
		} else {
			dispatch(actionAddIsFavorite(item.article));
		}
	}
	return (
		<div data-testid='favorites-page'>
			{favorites.map((el, index) => (
				<FavoritesCardsWrapper key={index} >
					<FavoritesCardImg src={el.url} alt={el.title} />
					<FavoritesCardTitle>{el.title}</FavoritesCardTitle>
					<FavoritesGoodsColor>Колір: {el.color}</FavoritesGoodsColor>
					<FavoritesCardArt>Артикуль: {el.article}</FavoritesCardArt>
					<FavoritesCardPrice>Ціна: {el.price} грн.</FavoritesCardPrice>
					<FavoritesCardStar type='button' onClick={() => {
						hendlerFavorites(el)
					}}>
						<FavoritStar style={{ fill: "darkblue" }} />
					</FavoritesCardStar>
				</FavoritesCardsWrapper>
			)
			)}
		</div>
	)
}

Favorites.propTypes = {
	favorites: PropTypes.array,
}