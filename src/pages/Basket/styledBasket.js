import styled from 'styled-components';

const BasketWrapper = styled.div`
display: flex;
align-items: space-around;
justify-content: space-between;
`
const BasketCardsWrapper = styled.div`
display:-webkit-box;
align-items: center;
justify-content: space-around;
gap: 50px;
margin: 20px;
box-shadow: 5px 5px 10px grey;
border-radius: 5px;
`

export { BasketWrapper, BasketCardsWrapper }