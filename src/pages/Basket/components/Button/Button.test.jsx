import { render, screen, renderHook } from '@testing-library/react';
import { toBeInTheDocument } from '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

import DisplayProvider from '../../../../context/DisplayProvaider';
import Button from './Button';


describe('component Button', () => {
	it('snapshot component Button', () => {
		const button = render(
			<DisplayProvider>
				<Button />
			</DisplayProvider>
		);

		expect(button).toMatchSnapshot();
	});

	it('click button change display', () => {
		render(
			<DisplayProvider>
				<Button />
			</DisplayProvider>
		);
		expect(screen.queryByTestId('icons-tablet')).toBeNull();
		expect(screen.getByTestId('icons-cart')).toBeInTheDocument();

		userEvent.click(screen.getByTestId('icons-cart'));
		expect(screen.queryByTestId('icons-cart')).toBeNull();
		expect(screen.getByTestId('icons-tablet')).toBeInTheDocument();

		userEvent.click(screen.getByTestId('icons-tablet'));
		expect(screen.queryByTestId('icons-tablet')).toBeNull();
		expect(screen.getByTestId('icons-cart')).toBeInTheDocument();
	});
});