import { useDisplay } from '../../../../context/DisplayProvaider';

import { IconsCart, IconsTable } from './styledButton'



const Button = () => {
	const { isCart, setIsCart } = useDisplay()

	return (
		<button>
			{isCart ? <IconsTable data-testid='icons-tablet' onClick={() => setIsCart(!isCart)} /> : <IconsCart data-testid='icons-cart' onClick={() => setIsCart(!isCart)} />}
		</button>

	)
}

export default Button
