import { Formik, Form } from "formik";
import PropTypes from 'prop-types';
import { useDispatch } from "react-redux";

import { Input, InputTextarea, InputPhone } from '../../../../components/Form';
import { validationSchema } from "./validation";
import { actionRemoveLocalstorageIsBasket } from '../../../../features/basket/basketSlice';

import { SidebarWrapper, FildsetStyled, LegendStyled, ButtonForm } from './styledSidebar';

export default function Sidebar({ basket }) {
	const dispatch = useDispatch()

	return (
		<SidebarWrapper>
			<Formik

				initialValues={
					{
						name: '',
						lastName: '',
						age: '',
						shippingAddress: '',
						phone: ''
					}
				}
				onSubmit={(values, actions) => {
					dispatch(actionRemoveLocalstorageIsBasket(basket));
					console.log('Buyer =>', values);
					for (let i = 0; i < basket.length; i++) {
						console.log(`позиція ${i} =>`, basket[i].title);
					  }
					  actions.resetForm();
		
				}}
				validationSchema={validationSchema}
			>
				{({ errors, touched, getFieldProps }) => (
					<Form>
					<FildsetStyled>
						<LegendStyled>Замовлення</LegendStyled>
						<Input inputName={'name'} lable={'Ім\'я'} placeholder="ваше і'мя" error={errors.name && touched.name} />

						<Input inputName={'lastName'} lable={'Призвіще'} placeholder='Ваше призвіще' error={errors.lastName && touched.lastName} />

						<Input inputName={'age'} type={'number'} label={'Вік'} placeholder='Скільки вам років' error={errors.age && touched.age} />

						<InputTextarea lable={'Адреса доставки'} nameTextArea={'shippingAddress'} rows={8} cols={23} placeholder={'введіть адресу доставки'} error={errors.shippingAddress && touched.shippingAddress} />

						<InputPhone lable={'контактний номер телефону'} inputPhoneName={'phone'} error={errors.phone && touched.phone} />

						<ButtonForm type="submit">ЗАМОВИТИ !!!</ButtonForm>

					</FildsetStyled>

				</Form>
				)}

			</Formik>
		</SidebarWrapper>

	)
}

Sidebar.propTypes = {
	basket: PropTypes.array
}