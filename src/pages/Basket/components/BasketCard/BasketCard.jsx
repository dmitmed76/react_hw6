import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { selectIsModalTwo } from '../../../../store/selectors';
import { actionIsModalTwo } from '../../../../features/modal/isModalTwoSlice';
import { actionAddIsCurrentGoods } from '../../../../features/basket/basketSlice';

import {
	BasketCardTitle,
	BasketGoodsColor,
	BasketCardArt,
	BasketCardImg,
	BasketCardPrice,
	DeleteButton,
	Icons
} from './styledBasketCard'


const BasketCard = ({ el }) => {
	const dispatch = useDispatch();
	const isModalTwo = useSelector(selectIsModalTwo);

	const hendlerModalTwo = () => {
		dispatch(actionIsModalTwo(!isModalTwo))
	}

	const hendlerCurrentGoods = (currentGoods) => {
		dispatch(actionAddIsCurrentGoods(currentGoods))
	}
	return (
		<>
			<BasketCardImg src={el.url} alt={el.title} />
			<BasketCardTitle>{el.title}</BasketCardTitle>
			<BasketGoodsColor>Колір: {el.color}</BasketGoodsColor>
			<BasketCardArt>Артикуль: {el.article}</BasketCardArt>
			<BasketCardPrice>Ціна: {el.price} грн.</BasketCardPrice>
			<DeleteButton data-testid='delete-basket' type="button" onClick={() => {
				hendlerModalTwo()
				hendlerCurrentGoods(el)
			}} >
				<Icons />
			</DeleteButton>
		</>
	)
}

BasketCard.propTypes = {
	item: PropTypes.object
}

export default BasketCard
