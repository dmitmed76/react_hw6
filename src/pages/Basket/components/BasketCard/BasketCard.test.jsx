import { render, screen } from '@testing-library/react';
import * as reduxHooks from 'react-redux';
import userEvent from '@testing-library/user-event';

import BasketCard from './BasketCard';
import * as actionModalTwo from '../../../../features/modal/isModalTwoSlice';
import * as actionsBasket from '../../../../features/basket/basketSlice';

jest.mock('react-redux');

const dispatch = jest.fn();
const mockedUseSelektor = jest.spyOn(reduxHooks, 'useSelector');
const mockedUseDispatch = jest.spyOn(reduxHooks, 'useDispatch');

const mockCard = {
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
};

describe('BasketCard', () => {
	it('should create empty basket', () => {
		mockedUseSelektor.mockReturnValue([]);
		const component = render(<BasketCard el={[]} />
		);

		expect(component).toMatchSnapshot();
	});

	it('should create basket', () => {
		mockedUseSelektor.mockReturnValue([mockCard]);
		const component = render(<BasketCard el={mockCard} />);

		expect(component).toMatchSnapshot();
	});

	it('should action open modal window "actionIsModalTwo" ', () => {
		mockedUseSelektor.mockReturnValue([mockCard]);
		mockedUseDispatch.mockReturnValue(dispatch);

		const mockedActionIsModalTwo = jest.spyOn(actionModalTwo, 'actionIsModalTwo');
		const mockedAddIsCurrentGoods = jest.spyOn(actionsBasket, 'actionAddIsCurrentGoods');
		render(<BasketCard el={mockCard} />);
		userEvent.click(screen.getByTestId('delete-basket'));

		expect(dispatch).toHaveBeenCalledTimes(2);
		expect(mockedActionIsModalTwo).toHaveBeenCalledWith(false);
		expect(mockedAddIsCurrentGoods).toHaveBeenCalledWith(mockCard);
	});
});