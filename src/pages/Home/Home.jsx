import { GoodsCards } from '../../components/GoodsCards';
import { useDisplay} from '../../context/DisplayProvaider'
import { IconsCart, IconsTable } from '../../components/GoodsCards/components/Button/styledButton'

export default function Home() {
	const { isCart, setIsCart } = useDisplay();
	return (
		<div data-testid='home-page'>
			<button>
			{isCart ? <IconsTable data-testid='icons-tablet' onClick={() => setIsCart(!isCart)} /> : <IconsCart data-testid='icons-cart' onClick={() => setIsCart(!isCart)} />}
		</button>
			<GoodsCards isCart={isCart ? '1fr' : 'repeat(4, 1fr)'}/>
		</div>


	)
}
