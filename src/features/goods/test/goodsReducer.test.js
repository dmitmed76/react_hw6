import goodsSlice, { getGoods } from "../goodsSlice";

const initialState = {
	projectors: [],
	status: null,
	error: null
}

describe('goodsSlice', () => {
	it('should change status with "getGoods.pending" action', () => {
		const state = goodsSlice(initialState, getGoods.pending());

		expect(state.status).toBe('loading');
		expect(state.error).toBeNull();
	});

	it('should fetch goods with "getGoods.fulfilled" action', () => {
		const mockCard = [{
			"title": "TouYinger M4 FullHD", "price": 3700,
			"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
			"article": 12345, "color": "White", "isFavorite": false
		}]
		const state = goodsSlice(initialState, getGoods.fulfilled(mockCard));

		expect(state).toEqual({
			projectors: mockCard,
			status: 'resolved',
			error: null
		});
	});

	it('should change status and error with "getGoods.rejected" action', () => {
		const action = {
			type: getGoods.rejected.type,
			payload: 'Server Error !'
		}
		const state = goodsSlice(initialState, action);

		expect(state).toEqual({
			projectors: [],
			status: 'rejected',
			error: 'Server Error !'
		});
	});
});

