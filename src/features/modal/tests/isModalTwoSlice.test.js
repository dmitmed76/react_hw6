import isModalTwoSlice, { actionIsModalTwo } from '../isModalTwoSlice';

const initialState = {
	isModalTwo: false,
}

describe('isModal', () => {

	it('should calling a modal window "actionIsModalTwo" action', () => {
		const action = { type: actionIsModalTwo.type, payload: true };
		const result = isModalTwoSlice(initialState, action);

		expect(result.isModalTwo).toEqual(true);
	});
});