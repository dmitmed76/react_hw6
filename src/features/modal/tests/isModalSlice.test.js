import isModalSlice, { actionIsModal } from '../isModalSlice';

const initialState = {
	isModal: false,
}

describe('isModal', () => {

	it('should calling a modal window "actionIsModal" action', () => {
		const action = { type: actionIsModal.type, payload: true };
		const result = isModalSlice(initialState, action);

		expect(result.isModal).toEqual(true);
	});
});